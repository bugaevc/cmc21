import asyncio
import signal
import sys
import os
import json

import digitex_engine_client
from digitex_bot_framework import *

with open(os.environ['CONFIG_PATH']) as config_file:
    config = json.load(config_file)

Bot.client_class = digitex_engine_client.MqClient

market = Market.AAPL

order = None
side = None

async def place_order():
    global order, side
    side = OrderSide.SELL if side is OrderSide.BUY else OrderSide.BUY
    order = Order(price=None, quantity=1000, side=side, type=OrderType.MARKET)
    print('Placing', order)
    await market.trader.orders.place(order)

async def on_order_update(this_order):
    global order
    print('Order', this_order.id, 'is', this_order.status)
    if this_order is not order:
        return
    if order.status is OrderStatus.REJECTED:
        await asyncio.sleep(1)
        await place_order()
    elif order.status in (OrderStatus.FILLED, OrderStatus.PARTIAL):
        await place_order()

Order.on_update = on_order_update

async def main():
    asyncio.get_running_loop().add_signal_handler(signal.SIGTERM, sys.exit)
    bot = Bot(
        trader_id=config['trader_id'],
        host=os.environ['MQ_HOST'],
        login=os.environ['MQ_USER'],
        password=os.environ['MQ_PASS'],
        virtualhost=os.environ['MQ_VHOST'],
    )
    await bot.add_market(market)
    await place_order()

asyncio.ensure_future(main())
asyncio.get_event_loop().run_forever()
