import digitex_engine_client
from digitex_bot_framework import *

import sys
import os
import signal
import asyncio
import json
import random
from decimal import Decimal


with open(os.environ['CONFIG_PATH']) as config_file:
    config = json.load(config_file)


Bot.client_class = digitex_engine_client.MqClient


market = Market.AAPL
trader = market.trader


async def place_an_order():
    last_trade = market.last_trade
    rounded_spot_price = market.rounded_spot_price()
    if rounded_spot_price is None or last_trade is None:
        print('Skipped placing an order due to unknown price', flush=True)
        return

    quantity = random.randint(
        config['min_quantity'],
        config['max_quantity'],
    )

    cancel_threshold = config['cancel_threshold']
    for order in trader.orders:
        if abs(order.price - rounded_spot_price) >= cancel_threshold:
            await order.cancel()

    if last_trade.price and rounded_spot_price < last_trade.price:
        side = OrderSide.SELL
    elif last_trade.price and rounded_spot_price > last_trade.price:
        side = OrderSide.BUY
    else:
        if trader.position.type == PositionType.LONG:
            side = OrderSide.SELL
        elif trader.position.type == PositionType.SHORT:
            side = OrderSide.BUY
        else:
            side = random.choice([OrderSide.BUY, OrderSide.SELL])

    reverse_probability = config['reverse_probability']
    if random.random() * 100 <= reverse_probability:
        side = OrderSide.BUY if side is OrderSide.SELL else OrderSide.BUY

    print(f'Placing an order for {quantity} at {rounded_spot_price}', flush=True)
    order = Order(price=rounded_spot_price, quantity=quantity, side=side)
    await trader.orders.place(order)


def on_order_update(order):
    print(order, 'is', order.status, flush=True)
    print('error code', order.error_code, flush=True)
    if order.status is OrderStatus.REJECTED:
        await trader.orders.cancel_all()


Order.on_update = on_order_update


async def on_currency_pair_update():
    print('Mark price', market.currency_pair.mark_price, flush=True)
    if market.last_trade is not None and market.rounded_spot_price() != market.last_trade.price:
        # Better act fast!
        await place_an_order()


market.currency_pair.on_update = on_currency_pair_update


async def main():
    asyncio.get_running_loop().add_signal_handler(signal.SIGTERM, sys.exit)
    bot = Bot(
        trader_id=config['trader_id'],
        host=os.environ['MQ_HOST'],
        login=os.environ['MQ_USER'],
        password=os.environ['MQ_PASS'],
        virtualhost=os.environ['MQ_VHOST'],
    )

    await bot.add_market(market)
    await trader.orders.cancel_all()

    while True:
        await asyncio.sleep(config['interval'])
        await place_an_order()

asyncio.run(main())
