import asyncio
import os
import signal
import logging

logger = logging.getLogger(__name__)

__all__ = ('ChildProcess',)

class ChildProcess:
    children_by_pid = dict()

    set_up_signal_handler = False
    def ensure_signal_handler():
        if ChildProcess.set_up_signal_handler:
            return
        loop = asyncio.get_running_loop()
        loop.add_signal_handler(signal.SIGCHLD, ChildProcess.reap_children)
        ChildProcess.set_up_signal_handler = True

    def __init__(self):
        self.pid = None
        self.exit_code = None
        self.exit_event = asyncio.Event()
        self.pipe_reader = None
        ChildProcess.ensure_signal_handler()

    def spawn(self, child_setup):
        assert self.pid is None
        assert self.pipe_reader is None
        pipe_reader, pipe_writer = os.pipe()
        self.pid = os.fork()
        if self.pid == 0:
            os.dup2(pipe_writer, 1)
            os.dup2(pipe_writer, 2)
            child_setup()
            logger.error('Returned from child_setup()')
            os._exit(1)

        ChildProcess.children_by_pid[self.pid] = self
        logger.info('Spawned as PID %d', self.pid)
        os.close(pipe_writer)
        os.set_blocking(pipe_reader, False)
        self.pipe_reader = open(pipe_reader)

    def did_exit(self, exit_code):
        assert self.pid is not None
        del ChildProcess.children_by_pid[self.pid]
        self.exit_code = exit_code
        self.exit_event.set()

    async def kill(self):
        assert self.pid is not None
        logger.info('Killing PID %d with SIGTERM', self.pid)
        os.kill(self.pid, signal.SIGTERM)
        logger.info('Waiting for it to exit...')
        try:
            await asyncio.wait_for(self.exit_event.wait(), timeout=2.0)
        except asyncio.TimeoutError:
            logger.info('Timed out waiting, killing with SIGKILL')
            os.kill(self.pid, signal.SIGKILL)
            await self.exit_event.wait()

    def reap_children():
        logger.info('Reaping children...')
        reaped_children_count = 0

        while True:
            try:
                siginfo = os.waitid(os.P_ALL, 0, os.WEXITED | os.WNOHANG)
            except ChildProcessError:
                break
            if siginfo is None:
                break
            reaped_children_count += 1
            pid = siginfo.si_pid
            status = siginfo.si_status
            logger.info('PID %d has exited with code %d', pid, status)
            try:
                child = ChildProcess.children_by_pid[pid]
            except KeyError:
                logger.warning('PID %d is not a known child process', pid)
            else:
                child.did_exit(status)

        if reaped_children_count > 1:
            logger.info('Reaped %d children', reaped_children_count)
        elif reaped_children_count == 0:
            logger.warn('There was no children to reap!')
