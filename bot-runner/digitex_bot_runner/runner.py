import os
import logging
import asyncio
import pathlib

import aiohttp

from .bot import Bot


logger = logging.getLogger(__name__)


__all__ = ('Runner',)


class Runner:
    def __init__(self):
        self.base_url = os.environ['BOT_MANAGER_URL']
        self.name = os.environ.get('BOT_RUNNER_NAME', os.uname().nodename)
        self.capacity = os.environ['BOT_RUNNER_CAPACITY']
        self.bot_impls_path = pathlib.Path(os.environ['BOT_IMPLS_PATH'])
        self.client = aiohttp.ClientSession()
        self.runner_id = None

    async def obtain_runner_id(self):
        logger.info('Registering the runner with the bot manager')
        url = self.base_url + 'runners/'
        data = {
            'name': self.name,
            'capacity': self.capacity,
        }
        async with self.client.post(url, data=data) as response:
            response.raise_for_status()
            json = await response.json()
            self.runner_id = int(json['id'])
            logger.info('Our runner ID is %s', self.runner_id)

    async def on_bot_changed_status(self, bot, status):
        url = self.base_url + f'bots/{bot.id}/changed-status'
        data = {
            'runner': self.runner_id,
            'status': {
                'stopped': 0,
                'running': 3,
                'failed': 6,
            }[status]
        }
        async with self.client.post(url, data=data) as response:
            response.raise_for_status()

    async def on_bot_output(self, bot, output):
        url = self.base_url + f'bots/{bot.id}/output'
        async with self.client.post(url, data=output) as response:
            response.raise_for_status()

    def make_bot(self, id, algorithm_path):
        impl_path = self.bot_impls_path / algorithm_path
        bot = Bot(id=id, argv=['python3', impl_path])
        bot.on_changed_status = lambda status: asyncio.ensure_future(self.on_bot_changed_status(bot, status))
        bot.on_output = lambda output: asyncio.ensure_future(self.on_bot_output(bot, output))
        return bot

    async def handle_action(self, action):
        id = action['id']
        try:
            status = action['status']
            algorithm_path = action['algorithm_path']
            config = action['config']
            if status == 2:
                # STOPPING
                try:
                    bot = Bot.bots_by_id[id]
                except KeyError:
                    logger.error('No bot with ID %s, nothing to stop', id)
                    return
                await bot.kill()
            elif status == 5:
                # STARTING
                if id not in Bot.bots_by_id:
                    bot = self.make_bot(id, algorithm_path)
                else:
                    bot = Bot.bots_by_id[id]
                bot.set_config(config)
                bot.up()
            else:
                logger.warning("Don't know what to do with status %s", status)
                return
        except:
            logger.exception('Failed to handle action for bot ID %s', id)
            url = self.base_url + f'bots/{id}/changed-status'
            data = {
                'runner': self.runner_id,
                'status': 6
            }
            await self.client.post(url, data=data)

    async def check_in(self):
        url = self.base_url + f'runners/{self.runner_id}/check-in'
        data = {
            'max_new_jobs': 42,
        }
        async with self.client.post(url, data=data) as response:
            response.raise_for_status()
            actions = await response.json()
        for action in actions:
            await self.handle_action(action)

    async def run(self):
        if self.runner_id is None:
            await self.obtain_runner_id()
        while True:
            try:
                await self.check_in()
            except:
                logger.exception('Failed to check in')
            await asyncio.sleep(5)
