import os
import logging
import ctypes
import ctypes.util
from pathlib import Path


logger = logging.getLogger(__name__)

CGROUP_ROOT = Path('/sys/fs/cgroup')
libc = ctypes.CDLL(ctypes.util.find_library('c'), use_errno=True)

MNT_DETACH = 2

MS_RDONLY = 1
MS_NOSUID = 2
MS_REMOUNT = 32
MS_BIND = 4096

CLONE_NEWPID = 0x20000000
CLONE_NEWUTS = 0x04000000
CLONE_NEWNS = 0x00020000
CLONE_NEWIPC = 0x08000000

def cvt(value, bad=-1):
    """Convert a C return value (with errno) into an appropriate RuntimeError, if needed"""
    if value == bad:
        errno = ctypes.get_errno()
        raise OSError(errno, os.strerror(errno))
    return value


class NewPidNamespace:
    def __enter__(self):
        self.current_namespace = open('/proc/self/ns/pid')
        cvt(libc.unshare(CLONE_NEWPID))
        return self

    def __exit__(self, *args):
        cvt(libc.setns(self.current_namespace.fileno(), CLONE_NEWPID))
        self.current_namespace.close()


class ControlGroup:
    def __init__(self, name):
        self.name = name
        self.path = CGROUP_ROOT / name

    def create(self):
        os.mkdir(self.path)

    def remove(self):
        os.rmdir(self.path)

    def move_process(self, pid=None):
        if pid is None:
            pid = os.getpid()
        procs = self.path / 'cgroup.procs'
        with open(procs, 'w') as procs:
            print(pid, file=procs)


def setup_namespace():
    try:
        cvt(libc.unshare(CLONE_NEWUTS | CLONE_NEWNS | CLONE_NEWIPC))
    except:
        logger.exception('Failed to unshare namespaces')
        return

    try:
        cvt(libc.umount2(b'/proc', MNT_DETACH))
        cvt(libc.mount(b'proc', b'/proc', b'proc', 0, 0))

        cvt(libc.umount2(b'/sys', MNT_DETACH))
        cvt(libc.mount(b'/', b'/', b'', MS_REMOUNT | MS_BIND | MS_RDONLY | MS_NOSUID, 0))
    except:
        logger.exception('Failed to unmount file systems')


def drop_root(uid, gid):
    try:
        # Drop GID first, because we can't do it after dropping UID.
        os.setgid(gid)
        os.setuid(uid)
    except:
        logger.exception('Failed to drop root')
