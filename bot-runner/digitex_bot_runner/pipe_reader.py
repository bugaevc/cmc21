import asyncio

class PipeReader:
    def __init__(self, file):
        self.file = file
        asyncio.ensure_future(self.collect())

    async def on_data(self):
        # To be hooked.
        pass

    async def collect(self):
        event = asyncio.Event()
        loop = asyncio.get_running_loop()
        loop.add_reader(self.file, event.set)
        try:
            while True:
                await event.wait()
                event.clear()
                try:
                    data = self.file.read()
                except TypeError:
                    # This is EWOULDBLOCK.
                    continue
                if not data:
                    # EOF.
                    break
                await self.on_data(data)
        finally:
            loop.remove_reader(self.file)
            self.file.close()
