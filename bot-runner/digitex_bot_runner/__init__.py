import asyncio
import logging

logging.basicConfig(level=logging.INFO)

__package__ = 'digitex_bot_runner'
from .runner import Runner

async def main():
    runner = Runner()
    await runner.run()

if __name__ == '__main__':
    asyncio.run(main())
