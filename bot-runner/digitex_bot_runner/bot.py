import asyncio
import os
import logging
import tempfile
import json
import random

from .child_process import ChildProcess
from .pipe_reader import PipeReader
from .sandbox import setup_namespace, drop_root, NewPidNamespace, ControlGroup

logger = logging.getLogger(__name__)

__all__ = ('Bot',)

class Bot:
    bots_by_id = dict()

    def __init__(self, /, id, argv):
        Bot.bots_by_id[id] = self
        self.id = id
        self.argv = argv
        self.child_process = None
        self.trying_to_kill = False
        self.cgroup = ControlGroup(name=f'bot-{id}')

        self.uid_gid = random.randint(1300, 1400)
        self.config_tmp_file = tempfile.NamedTemporaryFile(mode='w', prefix=f'bot-{id}-config-')
        os.chown(self.config_tmp_file.name, self.uid_gid, self.uid_gid)
        logger.info('Config file for bot %s is %s', id, self.config_tmp_file.name)

    def set_config(self, config):
        self.config_tmp_file.truncate(0)
        self.config_tmp_file.seek(0, 0)
        json.dump(config, self.config_tmp_file)
        self.config_tmp_file.flush()

    def on_changed_status(self, status):
        # To be hooked from Runner.
        pass

    async def on_output(self, output):
        # To be hooked form Runner.
        pass

    def child_setup(self):
        try:
            self.cgroup.create()
            self.cgroup.move_process()
        except:
            logger.exception('Failed to set up a control group')
        setup_namespace()
        drop_root(self.uid_gid, self.uid_gid)
        os.environ['CONFIG_PATH'] = self.config_tmp_file.name
        os.execvp(self.argv[0], self.argv)

    def spawn(self):
        assert self.child_process is None or self.child_process.pid is None
        logger.info('Spawning bot %s with argv %s', self.id, self.argv)
        self.trying_to_kill = False

        with NewPidNamespace():
            self.child_process = ChildProcess()
            self.child_process.spawn(self.child_setup)

        pipe_reader = PipeReader(file=self.child_process.pipe_reader)
        pipe_reader.on_data = self.on_output

    def up(self):
        if self.child_process is None:
            self.spawn()
            asyncio.ensure_future(self.watch_and_report())

    async def watch_and_report(self):
        assert self.child_process is not None
        await asyncio.sleep(1)
        if self.child_process and self.child_process.exit_code is None and not self.trying_to_kill:
            self.on_changed_status('running')
        await self.child_process.exit_event.wait()
        try:
            self.cgroup.remove()
        except:
            logger.exception('Failed to clean up a control group')
        if self.trying_to_kill or self.child_process.exit_code == 0:
            self.on_changed_status('stopped')
        else:
            self.on_changed_status('failed')

    async def kill(self):
        assert self.child_process is not None
        self.trying_to_kill = True
        await self.child_process.kill()
        self.trying_to_kill = False
        self.child_process = None

