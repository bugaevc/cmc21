import os
import logging
import asyncio

import aiohttp


logger = logging.getLogger(__name__)


class Bot:
    def __init__(self, id, supervisor):
        self.id = id
        self.supervisor = supervisor


    @staticmethod
    async def create(supevisor, trader_id, algorithm_id, config):
        url = self.supervisor.bot_manager_url + 'bots/'
        data = {
            'trader_id': trader_id,
            'config': config,
            'algorithm': algorithm_id,
        }
        async with self.supervisor.client.post(url, data=data) as response:
            response.raise_for_status()
            json = await response.json()
            return Bot(id=json['id'], supervisor=supervisor)


    async def give_balance(self):
        url = self.supervisor.bot_manager_url + f'bots/{self.id}/balance'
        async with self.supervisor.client.post(url) as response:
            response.raise_for_status()


    async def start(self):
        url = self.supervisor.bot_manager_url + f'bots/{self.id}/start'
        async with self.supervisor.client.post(url) as response:
            response.raise_for_status()


    async def stop(self):
        url = self.supervisor.bot_manager_url + f'bots/{self.id}/stop'
        async with self.supervisor.client.post(url) as response:
            response.raise_for_status()


class Supervisor:
    def __init__(self):
        self.bot_manager_url = os.environ['BOT_MANAGER_URL']
        self.client = aiohttp.ClientSession()
        self.next_trader_id = 100


    async def register_simple(self):
        url = self.bot_manager_url + 'algorithms/'
        data = {
            'name': 'Simple',
            'impl_path': 'simple.py',
            'default_config': {
                'interval': 5,
                'min_quantity': 100,
                'max_quantity': 500,
                'reverse_probability': 20,
                'cancel_threshold': 5,
            },
            'description': 'Simple scalping algorithm',
        }
        async with self.client.post(url, data=data) as response:
            response.raise_for_status()
            json = await response.json()
            return json['id']


    async def make_bot(self, algorithm_id, config):
        trader_id = self.next_trader_id
        self.next_trader_id += 1
        bot = await Bot.create(self, trader_id, algorithm_id, config)
        await bot.give_balance()
        return bot


async def main():
    supervisor = Supervisor()
    simple_id = await supervisor.register_simple()
    for interval in range(1, 10):
        config = {
            'interval': interval,
            'min_quantity': 100,
            'max_quantity': 500,
            'reverse_probability': 20,
            'cancel_threshold': 5,
        }
        bot = await supervisor.make_bot(simple_id, config)
        await bot.start()


if __name__ == '__main__':
    asyncio.run(main())
