import asyncio
import os
import math
import random
from decimal import Decimal

import aiohttp

__package__ = 'price_updater'
from .finnhub import FinnhubClient
from .engine import EngineClient
from .event import Event


async def from_finnhub(engine_client):
    async with aiohttp.ClientSession() as session:
        finnhub_client = FinnhubClient(session=session)
        await finnhub_client.connect()
        await finnhub_client.subscribe('AAPL')
        async for event in finnhub_client.events():
            await engine_client.send_event(event)


def decimalize(f):
    return Decimal(f).quantize(Decimal('0.01'))


async def synthetic_sin(engine_client):
    x = 0.0
    while True:
        price = decimalize(math.sin(x) * 100 + 1000)
        await engine_client.send_event(
            Event(
                symbol='AAPL',
                price=price,
                volume=Decimal(1),
            )
        )
        x += 0.5
        await asyncio.sleep(1)


async def synthetic_random_walk(engine_client):
    price = Decimal('1000.00')
    while True:
        price += decimalize(random.random()) - Decimal('0.5')
        if price < 500:
            continue
        await engine_client.send_event(
            Event(
                symbol='AAPL',
                price=price,
                volume=Decimal(1),
            )
        )
        await asyncio.sleep(1)


async def main():
    engine_client = EngineClient()
    if os.environ['PRICE_UPDATER_SOURCE'] == 'realtime':
        await from_finnhub(engine_client)
    elif os.environ['PRICE_UPDATER_SOURCE'] == 'historic':
        print('TODO')
        return
    elif os.environ['PRICE_UPDATER_SOURCE'] == 'synthetic-sin':
        await synthetic_sin(engine_client)
    elif os.environ['PRICE_UPDATER_SOURCE'] == 'synthetic-random-walk':
        await synthetic_random_walk(engine_client)
    else:
        raise ValueError('No PRICE_UPDATER_SOURCE')

asyncio.run(main())
