import os
from decimal import Decimal

import digitex_engine_client


def decimal_to_proto(decimal):
    if decimal is None:
        return None
    if not isinstance(decimal, Decimal):
        decimal = Decimal(decimal)
    negative, digits, exponent = decimal.as_tuple()
    value = 0
    for digit in digits:
        value *= 10
        value += digit
    if negative:
        value = -value
    scale = -exponent
    return digitex_engine_client.messages_pb2.Decimal(value64=value, scale=scale)


class EngineClient:
    def __init__(self):
        self.client = digitex_engine_client.MqClient(
            host=os.environ['MQ_HOST'],
            login=os.environ['MQ_USER'],
            password=os.environ['MQ_PASS'],
            virtualhost=os.environ['MQ_VHOST'],
            target_exchange=os.environ['MQ_PU_DATA_EXCHANGE'],
            target_routing_key='',
        )

    async def send_event(self, event):
        print('Sending event:', event.price, event.volume)
        await self.client.exchange_rate_update(
            currency_pair_id=19,
            volume=decimal_to_proto(event.price * event.volume),
            quantity=decimal_to_proto(event.volume),
        )
