import os
import json
from decimal import Decimal

import aiohttp

from .event import Event


class FinnhubClient:
    def __init__(self, session):
        self.session = session
        self.ws = None

    async def connect(self):
        token = os.environ['FINNHUB_TOKEN']
        url = 'wss://ws.finnhub.io/?token=' + token
        print('Connecting to Finnhub...')
        self.ws = await self.session.ws_connect(url)
        print('Connected')

    async def subscribe(self, symbol):
        print('Subscribing to', symbol)
        await self.ws.send_json({
            'type': 'subscribe',
            'symbol': symbol
        })

    async def events(self):
        async for message in self.ws:
            print('Got a message', message)
            if message.type == aiohttp.WSMsgType.CLOSE or message.type >= 0x100:
                raise StopAsyncIteration()
            for event in self.parse_message(message):
                yield event

    def parse_message(self, message):
        def loads(data):
            return json.loads(data, parse_float=Decimal)

        data = message.json(loads=loads)
        if data['type'] != 'trade':
            return
        for item in data['data']:
            yield Event(
                symbol=item['s'],
                price=Decimal(item['p']),
                volume=Decimal(item['v']),
            )
