class Event:
    def __init__(self, /, symbol, price, volume):
        self.symbol = symbol
        self.price = price
        self.volume = volume

    def __repr__(self):
        return f'Event(symbol={self.symbol}, price={self.price}, volume={self.volume})'
