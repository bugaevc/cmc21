#!/bin/bash
set -e

./manage.py migrate
exec gunicorn --bind=0.0.0.0:80 bot_manager.wsgi
