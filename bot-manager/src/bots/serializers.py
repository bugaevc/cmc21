from rest_framework import serializers

from .models import *


__all__ = (
    'AlgorithmSerializer',
    'RunnerSerializer',
    'BotSerializer',
    'RunnerCheckInSerializer',
    'BotSetConfigSerializer',
    'BotChangedStatusSerializer',
)


class AlgorithmSerializer(serializers.ModelSerializer):
    class Meta:
        model = Algorithm
        fields = '__all__'
        read_only_fields = ['id']


class RunnerSerializer(serializers.ModelSerializer):
    class Meta:
        model = Runner
        fields = '__all__'
        read_only_fields = ['id', 'last_seen']


class BotSerializer(serializers.ModelSerializer):
    class Meta:
        model = Bot
        fields = [
            'id', 'trader_id',
            'config', 'status', 'last_status_change_time',
            'algorithm', 'algorithm_path',
            'runner', 'runner_name', 'is_active', 'creation_datetime',
            'deactivation_datetime', 'output'
        ]
        read_only_fields = [
            'id', 'status', 'last_status_change_time', 'runner', 'is_active',
            'creation_datetime', 'deactivation_datetime', 'output'
        ]

    algorithm_path = serializers.CharField(source='algorithm.impl_path', read_only=True)
    runner_name = serializers.CharField(source='runner.name', read_only=True)


class RunnerCheckInSerializer(serializers.Serializer):
    max_new_jobs = serializers.IntegerField(required=False)



class BotSetConfigSerializer(serializers.Serializer):
    config = serializers.JSONField()


class BotChangedStatusSerializer(serializers.Serializer):
    runner = serializers.PrimaryKeyRelatedField(queryset=Runner.objects.all())
    status = serializers.ChoiceField(choices=Bot.Status.CHOICES)
