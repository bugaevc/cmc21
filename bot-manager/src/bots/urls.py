from django.urls import path

from . import views

urlpatterns = [
    path('algorithms/', views.AlgorithmsList.as_view()),
    path('algorithms/<int:pk>', views.AlgorithmDetail.as_view()),
    path('runners/', views.RunnersList.as_view()),
    path('runners/<int:pk>', views.RunnerDetail.as_view()),
    path('runners/<int:pk>/check-in', views.RunnerCheckIn.as_view()),
    path('bots/', views.ActiveBotsList.as_view()),
    path('bots/all', views.AllBotsList.as_view()),
    path('bots/<int:pk>', views.BotDetail.as_view()),
    path('bots/<int:pk>/start', views.BotStart.as_view()),
    path('bots/<int:pk>/stop', views.BotStop.as_view()),
    path('bots/<int:pk>/set-config', views.BotSetConfig.as_view()),
    path('bots/<int:pk>/changed-status', views.BotChangedStatus.as_view()),
    path('bots/<int:pk>/output', views.BotOutput.as_view()),
    path('bots/<int:pk>/delete', views.BotDelete.as_view()),
    path('bots/<int:pk>/balance', views.BotBalance.as_view()),
]
