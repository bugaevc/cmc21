import django_filters
from bots.models import Bot


class BotsFilter(django_filters.FilterSet):
    trader_id = django_filters.NumberFilter(field_name='trader_id')
    algorithm = django_filters.NumberFilter(field_name='algorithm__id')
    status = django_filters.ChoiceFilter(choices=Bot.Status.CHOICES)
    is_active = django_filters.BooleanFilter(field_name='is_active')
    creation_from = django_filters.IsoDateTimeFilter(field_name='creation_datetime', lookup_expr='gte')
    creation_to = django_filters.IsoDateTimeFilter(field_name='creation_datetime', lookup_expr='lte')

    class Meta:
        model = Bot
        fields = [
            'trader_id',
            'algorithm__id',
            'status',
            'is_active',
            'creation_datetime',
        ]
