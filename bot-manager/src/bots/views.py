import os

import digitex_engine_client

from django_filters import rest_framework
from django.http import Http404
from django.utils.timezone import now
from django.db import transaction
from rest_framework.views import APIView
from rest_framework.generics import ListCreateAPIView, RetrieveUpdateDestroyAPIView, RetrieveAPIView
from rest_framework.response import Response
from rest_framework.exceptions import ValidationError
from rest_framework.parsers import BaseParser

from .filtersets import BotsFilter
from .paginators import BotsPaginator
from .models import *
from .serializers import *
from .engine_client import get_engine_client


class AlgorithmsList(ListCreateAPIView):
    queryset = Algorithm.objects.all()
    serializer_class = AlgorithmSerializer


class AlgorithmDetail(RetrieveUpdateDestroyAPIView):
    queryset = Algorithm.objects.all()
    serializer_class = AlgorithmSerializer


class RunnersList(ListCreateAPIView):
    queryset = Runner.objects.all()
    serializer_class = RunnerSerializer


class RunnerDetail(RetrieveUpdateDestroyAPIView):
    queryset = Runner.objects.all()
    serializer_class = RunnerSerializer


class RunnerCheckIn(APIView):
    serializer_class = RunnerCheckInSerializer

    def post(self, request, pk):
        try:
            runner = Runner.objects.get(id=pk)
        except Runner.DoesNotExist:
            raise Http404('Runner does not exist')
        runner.save()

        deserializer = self.serializer_class(data=request.data)
        deserializer.is_valid(raise_exception=True)

        with transaction.atomic():
            waiting_to_stop = Bot.objects.filter(
                runner=runner,
                status=Bot.Status.WAITING_TO_STOP
            )
            waiting_to_start = Bot.objects.filter(
                status=Bot.Status.WAITING_TO_START
            )
            if 'max_new_jobs' in deserializer.validated_data:
                max_new_jobs = deserializer.validated_data['max_new_jobs']
                waiting_to_start = waiting_to_start[:max_new_jobs]

            for bot in waiting_to_stop:
                bot.status = Bot.Status.STOPPING
                bot.last_status_change_time = now()
                bot.save()
            for bot in waiting_to_start:
                bot.status = Bot.Status.STARTING
                bot.last_status_change_time = now()
                bot.runner = runner
                bot.save()

            serializer = BotSerializer(list(waiting_to_stop) + list(waiting_to_start), many=True)
            return Response(serializer.data)


class ActiveBotsList(ListCreateAPIView):
    serializer_class = BotSerializer
    pagination_class = BotsPaginator
    filter_backends = (rest_framework.DjangoFilterBackend,)
    filterset_class = BotsFilter
    queryset = Bot.objects.filter(is_active=True)


class AllBotsList(ActiveBotsList):
    queryset = Bot.objects


class BotDetail(RetrieveAPIView):
    queryset = Bot.objects.all()
    serializer_class = BotSerializer


class BotAPIView(APIView):
    def post(self, request, pk, *args, **kwargs):
        try:
            bot = Bot.objects.get(id=pk)
        except Bot.DoesNotExist:
            raise Http404('Bot does not exist')
        return self.perform_action(request, bot, *args, **kwargs)


class BotStart(BotAPIView):
    def perform_action(self, request, bot):
        if bot.status not in (Bot.Status.STOPPED, Bot.Status.STOPPING, Bot.Status.FAILED):
            raise ValidationError(detail='Cannot start the bot in this state')

        if not bot.is_active:
            raise ValidationError(detail='Cannot start deleted bot')

        bot.config['trader_id'] = bot.trader_id

        bot.status = Bot.Status.WAITING_TO_START
        bot.last_status_change_time = now()
        bot.save()
        return Response()


class BotStop(BotAPIView):
    def perform_action(self, request, bot):
        if bot.status not in (Bot.Status.WAITING_TO_START, Bot.Status.STARTING, Bot.Status.RUNNING):
            raise ValidationError(detail='Cannot stop the bot in this state')
        bot.status = Bot.Status.WAITING_TO_STOP
        bot.last_status_change_time = now()
        bot.save()
        return Response()


class BotDelete(BotAPIView):
    def perform_action(self, request, bot: Bot):
        if bot.status not in (Bot.Status.STOPPED, Bot.Status.STOPPING, Bot.Status.FAILED):
            raise ValidationError(detail='Cannot delete the bot in this state')
        if not bot.is_active:
            raise ValidationError(detail='Already deleted.')
        bot.is_active = False
        bot.deactivation_datetime = now()
        bot.save()
        return Response()


class BotSetConfig(BotAPIView):
    serializer_class = BotSetConfigSerializer

    def perform_action(self, request, bot):
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)

        if bot.status not in (Bot.Status.STOPPED, Bot.Status.STOPPING, Bot.Status.FAILED):
            raise ValidationError(detail='Cannot change config in this state')

        if not bot.is_active:
            raise ValidationError(detail='Cannot set config of deleted bot')

        bot.config = serializer.validated_data['config']
        bot.save()
        return Response()


class BotChangedStatus(BotAPIView):
    serializer_class = BotChangedStatusSerializer

    def perform_action(self, request, bot, **kwargs):
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)

        runner = serializer.validated_data['runner']
        runner.save()

        bot.runner = runner
        bot.last_status_change_time = now()
        bot.status = serializer.validated_data['status']
        bot.save()
        return Response()


class PlainTextParser(BaseParser):
    media_type = 'text/plain'

    def parse(self, stream, media_type=None, parser_context=None):
        return stream.read().decode('utf-8', 'replace')


class BotOutput(BotAPIView):
    parser_classes = [PlainTextParser]

    def perform_action(self, request, bot, **kwargs):
        # Keep the last 10 KB of output.
        bot.output = (bot.output + request.data)[-10 * 1024:]
        bot.save()
        return Response()


class BotBalance(BotAPIView):
    def perform_action(self, request, bot):
        client = get_engine_client()
        client.trader_balance_update(
            trader_id=bot.trader_id,
            market_id=1,
            currency_id=1,
            increment=digitex_engine_client.messages_pb2.Decimal(value64=100_000_000_0000, scale=4),
        )
        return Response()
