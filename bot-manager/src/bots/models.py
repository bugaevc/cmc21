from django.db import models
from django.utils.timezone import now

__all__ = (
    'Algorithm',
    'Runner',
    'Bot',
)

class Algorithm(models.Model):
    def default_config_default():
        return dict()

    name = models.CharField(max_length=128)
    impl_path = models.CharField(max_length=128)
    default_config = models.JSONField(default=default_config_default)
    description = models.CharField(max_length=1000, default='')


class Runner(models.Model):
    last_seen = models.DateTimeField(auto_now=True)
    name = models.CharField(max_length=128)
    capacity = models.IntegerField()


class Bot(models.Model):
    class Status:
        STOPPED = 0
        WAITING_TO_STOP = 1
        STOPPING = 2
        RUNNING = 3
        WAITING_TO_START = 4
        STARTING = 5
        FAILED = 6

        CHOICES = [
            (STOPPED, 'Stopped'),
            (WAITING_TO_STOP, 'Waiting to stop'),
            (STOPPING, 'Stopping'),
            (RUNNING, 'Running'),
            (WAITING_TO_START, 'Waiting to start'),
            (STARTING, 'Starting'),
            (FAILED, 'Failed'),
        ]
        _default_value, _name = CHOICES[0]

    creation_datetime = models.DateTimeField(default=now)
    algorithm = models.ForeignKey(Algorithm, on_delete=models.PROTECT)
    trader_id = models.IntegerField()
    config = models.JSONField()
    status = models.IntegerField(choices=Status.CHOICES, default=Status.STOPPED)
    last_status_change_time = models.DateTimeField(null=True)
    runner = models.ForeignKey(Runner, null=True, on_delete=models.PROTECT)
    is_active = models.BooleanField(default=True)  # Indicates that bot is deleted by user.
    deactivation_datetime = models.DateTimeField(null=True)
    output = models.TextField()
