import os
import threading

import digitex_engine_client


__all__ = ('get_engine_client',)


engine_client = None
engine_client_lock = threading.Lock()


def get_engine_client():
    global engine_client

    with engine_client_lock:
        if engine_client is not None:
            return engine_client
        engine_client = digitex_engine_client.MqClient(
            host=os.environ['MQ_HOST'],
            login=os.environ['MQ_USER'],
            password=os.environ['MQ_PASS'],
            virtualhost=os.environ['MQ_VHOST'],
        )
        engine_client.ping()
        return engine_client
