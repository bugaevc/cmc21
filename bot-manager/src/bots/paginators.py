from common.pagination import DRFCursorPagination


class BotsPaginator(DRFCursorPagination):
    page_size = 100
    ordering = '-creation_datetime'
