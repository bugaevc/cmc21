import asyncio

__package__ = 'stats_collector'
from .engine import listen_for_events
from .ws import run_ws_app
from .stats import collect_stats


async def main():
    asyncio.ensure_future(run_ws_app())
    asyncio.ensure_future(collect_stats())
    await listen_for_events()


asyncio.run(main())
