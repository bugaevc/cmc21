import os

import digitex_engine_client

from .broadcast import EVENTS
from .utils import decimal_from_proto, datetime_from_proto


__all__ = ('listen_for_events',)


class Event:
    pass


async def listen_for_events():
    client = digitex_engine_client.MqClient(
        host=os.environ['MQ_HOST'],
        login=os.environ['MQ_USER'],
        password=os.environ['MQ_PASS'],
        virtualhost=os.environ['MQ_VHOST'],
    )

    async for message in await client.subscribe_to_trading_events():
        which_one = message.WhichOneof('kontent')
        if which_one == 'order_status_msg':
            event = Event()
            event.trader_id = message.trader_id
            event.balance = decimal_from_proto(message.order_status_msg.trader_balance)
            event.datetime = datetime_from_proto(message.timestamp)
            await EVENTS.emit(event)
        elif which_one == 'order_filled_msg':
            event = Event()
            event.trader_id = message.trader_id
            event.balance = decimal_from_proto(message.order_filled_msg.trader_balance)
            event.balance2 = decimal_from_proto(message.order_filled_msg.trader_balance_2)
            event.datetime = datetime_from_proto(message.timestamp)
            await EVENTS.emit(event)
