import aiohttp.web

from .broadcast import BroadcastConsumer, EVENTS


__all__ = ('run_ws_app',)


async def events(request):
    ws = aiohttp.web.WebSocketResponse()
    await ws.prepare(request)

    with BroadcastConsumer(EVENTS) as consumer:
        async for event in consumer:
            await ws.send_json({
                'type': 'balance',
                'trader_id': event.trader_id,
                'balance': str(event.balance),
                'datetime': str(event.datetime),
            })

    await ws.close()


async def run_ws_app():
    app = aiohttp.web.Application()
    app.add_routes([aiohttp.web.get('/', events)])
    await aiohttp.web._run_app(app, port=80)
