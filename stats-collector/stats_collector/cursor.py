__all__ = ('Cursor',)


class Cursor:
    def __init__(self, connection):
        self.connection = connection

    def __enter__(self):
        self.cursor = self.connection.cursor()
        return self.cursor

    def __exit__(self, *whatever):
        self.cursor.close()

