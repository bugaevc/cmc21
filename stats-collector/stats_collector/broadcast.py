import asyncio


__all__ = ('Broadcast', 'BroadcastConsumer', 'EVENTS')


class Broadcast:
    def __init__(self):
        self.consumers = []

    async def emit(self, message):
        awaitables = [
            consumer.put(message)
            for consumer in self.consumers
        ]
        await asyncio.gather(
            *awaitables,
            return_exceptions=False
        )


class BroadcastConsumer:
    def __init__(self, broadcast):
        self.broadcast = broadcast
        self.queue = asyncio.Queue()

    def __enter__(self):
        self.broadcast.consumers.append(self.queue)
        return self

    def __exit__(self, *whatever):
        self.broadcast.consumers.remove(self.queue)

    def __aiter__(self):
        return self

    async def __anext__(self):
        return await self.queue.get()


EVENTS = Broadcast()
