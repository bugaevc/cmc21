import math
import sqlite3
from decimal import Decimal

from .broadcast import BroadcastConsumer, EVENTS
from .cursor import Cursor


__all__ = ('collect_stats',)


async def collect_stats():
    connection = sqlite3.connect('stats.db')
    with connection:
        connection.execute(
            'CREATE TABLE IF NOT EXISTS balance('
            'trader_id integer, datetime datetime, balance decimal, balance2 decimal'
            ')'
        )
        connection.execute(
            'CREATE TABLE IF NOT EXISTS stats('
            'trader_id integer, datetime datetime, '
            'pnl decimal, drawdown decimal, max_drawdown decimal, sharpe decimal,'
            'turnover decimal, fee decimal, calmar decimal'
            ')'
        )
    with BroadcastConsumer(EVENTS) as consumer:
        async for event in consumer:
            with connection:
                trader_id = event.trader_id
                datetime = event.datetime
                balance = event.balance
                balance2 = event.balance2 if hasattr(event, 'balance2') else None
                connection.execute(
                    'INSERT INTO balance(trader_id, datetime, balance, balance2) VALUES(?, ?, ?, ?)',
                    (trader_id, datetime, str(balance), str(balance2))
                )
                with Cursor(connection) as cursor:
                    initial_balance = cursor.execute(
                        'SELECT balance FROM balance '
                        #'WHERE trader_id = ? AND datetime > datetime(?, "-10 seconds") '
                        'WHERE trader_id = ? '
                        'ORDER BY datetime LIMIT 1',
                        (trader_id,)
                    ).fetchone()[0]
                    initial_balance = Decimal(initial_balance)

                    pnl = event.balance - initial_balance

                    max_balance = cursor.execute(
                        'SELECT max(balance) FROM balance WHERE trader_id = ?',
                        (trader_id,)
                    ).fetchone()[0]
                    max_balance = Decimal(max_balance)

                    drawdown = max(max_balance - balance, Decimal(0))

                    max_drawdown = cursor.execute(
                        'SELECT MAX(b1.balance - b2.balance) FROM balance AS b1, balance AS b2 '
                        'WHERE b1.trader_id = b2.trader_id AND b1.trader_id = ? AND b1.datetime < b2.datetime',
                        (trader_id,)
                    ).fetchone()[0]

                    max_drawdown = Decimal(max_drawdown) if max_drawdown is not None else None

                    stddev2 = cursor.execute(
                        'SELECT AVG((balance - s.av) * (balance - s.av)) FROM balance, '
                        '(SELECT AVG(balance) as av from balance) AS s'
                    ).fetchone()[0]
                    stddev = math.sqrt(stddev2)

                    if stddev:
                        sharpe = float(pnl) / stddev
                    else:
                        sharpe = None

                    if max_drawdown:
                        calmar = float(pnl / max_drawdown)
                    else:
                        calmar = None

                    cursor.execute(
                        'INSERT INTO stats('
                        'trader_id, datetime, pnl, drawdown, max_drawdown, sharpe, turnover, fee, calmar'
                        ') VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?)',
                        (
                            trader_id,
                            datetime,
                            str(pnl),
                            str(drawdown) if drawdown is not None else None,
                            str(max_drawdown) if max_drawdown is not None else None,
                            sharpe,
                            None,
                            None,
                            calmar,
                        )
                    )
