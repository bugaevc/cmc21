import datetime
from decimal import Decimal

import pytz


__all__ = ('decimal_from_proto', 'datetime_from_proto')


def decimal_from_proto(proto_decimal):
    return Decimal(proto_decimal.value64).scaleb(-proto_decimal.scale)


def datetime_from_proto(timestamp):
    if timestamp == 0:
        return None
    timestamp /= 1_000_000
    return datetime.datetime.fromtimestamp(timestamp, tz=pytz.UTC)
